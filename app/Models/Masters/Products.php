<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Products extends Model
{
    protected $table = "msproduct";

    protected $fillable = [
        "typeid",
        "productcd",
        "productnm",
        "description"
    ];

    public $timestamps = false;

    public $defaultSelects = array(
        "productcd",
        "productnm",
        "description"
    );

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $products = new Products();
        return $products->withJoin(is_null($selects) ? $products->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|Products $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'parent' => function ($query) {
                Types::foreignSelect($query);
            }
        ])->select('id', 'typeid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|Products
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, is_array($selects) ? $selects : func_get_args());
    }

    public function parent()
    {
        return $this->hasOne(Types::class, 'id', 'typeid');
    }
}
