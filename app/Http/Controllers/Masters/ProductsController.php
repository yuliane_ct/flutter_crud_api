<?php

namespace App\Http\Controllers\Masters;

use Illuminate\Http\Request;
use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\Products;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    protected $products;

    public function __construct()
    {
        $this->products = new Products();
    }

    public function selectApi(Request $req)
    {
        try {
            $searchValue = trim(strtolower($req->searchValue));
            $query = $this->products->withJoin($this->products->defaultSelects)
                ->where(function ($query) use ($searchValue) {
                    /* @var Relation $query */
                    $query->where(DB::raw('productcd'), 'like', "%$searchValue%");
                    $query->orWhere(DB::raw('productnm'), 'like', "%$searchValue%");
                });

            if ($req->has('typeid') && !empty($req->typeid)) {
                $typeid = $req->post('typeid');
                $query->where('id', '!=', $typeid);
                $query->where('typeid', '!=', $typeid);
            }

            $json = array();
            foreach ($query->get() as $db) {
                $json[] = ['value' => $db->id, 'text' => $db->productnm];
            }

            return $this->jsonSuccess(null, $json);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function datatables(Request $req)
    {
        try {
            $query = $this->products->withJoin($this->products->defaultSelects);

            return $this->jsonSuccess(
                null,
                datatables()->eloquent($query)
                    ->with('start', intval($req->start))
                    ->toJson()
                    ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(Request $req)
    {
        try {
            $this->products->create($req->all());

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function show($id)
    {
        try {
            $row = $this->products->withJoin($this->products->defaultSelects)
                ->find($id);

            if (is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonSuccess(null, $row);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function update(Request $req, $id)
    {
        try {

            $row = $this->products->find($id);

            if (is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->update($req->all());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function destory($id)
    {
        try {

            $row = $this->products->find($id);

            if (is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}
