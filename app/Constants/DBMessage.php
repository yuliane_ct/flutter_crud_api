<?php


namespace App\Constants;


class DBMessage
{

    const SUCCESS = "Process telah berhasil";
    const SUCCESS_DELETED = "Data berhasil dihapus";
    const SUCCESS_ADD = "Data berhasil ditambahkan";
    const SUCCESS_EDIT = "Data berhasil diupdate";
    const SUCCESS_REPORTED = "Proses reporting berhasil";
    const SUCCESS_UPLOADED = "Data berhasil diupload";
    const USER_NOT_FOUND = "Nama Pengguna Tidak Ditemukan";

    const ERROR_CORRUPT_DATA = "Data tidak ditemukan";
    const DATA_EXIST = "Data %s sudah ada";
}
